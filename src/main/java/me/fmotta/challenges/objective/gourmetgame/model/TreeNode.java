package me.fmotta.challenges.objective.gourmetgame.model;

/**
 * Classe modelo de nós da arvore de conhecimento binária.
 *
 * <br/><br/>
 * Copyright © 2020 Antonio Motta
 */
public class TreeNode {

	/**
	 * Descrição.
	 */
	private String data;

	/**
	 * Nó da arvore binaria da esquerda.
	 */
	private TreeNode left; 
	
	/**
	 * Nó da arvore binaria da direita.
	 */
	private TreeNode right;

	/**
	 * Construtor padrão
	 */
	TreeNode() {}

	/**
	 * Construtor de nó da arvore com descrição.
	 *
	 * @param description descrição.
	 */
	public TreeNode(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}

	public void setData(String description) {
		this.data = description;
	}

	public TreeNode getLeft() {
		return left;
	}

	public void setLeft(TreeNode left) {
		this.left = left;
	}

	public TreeNode getRight() {
		return right;
	}

	public void setRight(TreeNode right) {
		this.right = right;
	}
}
