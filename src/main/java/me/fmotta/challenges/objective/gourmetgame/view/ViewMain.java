package me.fmotta.challenges.objective.gourmetgame.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import me.fmotta.challenges.objective.gourmetgame.constants.GourmetMessage;
import me.fmotta.challenges.objective.gourmetgame.constants.GourmetParam;
import me.fmotta.challenges.objective.gourmetgame.controller.GourmetGameController;

/**
 * Classe principal da camada de vizualização.
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */
public class ViewMain {

	/**
	 * Metodo de inicialização da janela.
	 */
	public void start() {

		GourmetGameController controller = new GourmetGameController();

		JFrame frame = new JFrame(GourmetMessage.NAME_GAME.getValue());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		frame.setSize(new Dimension(GourmetParam.WINDOW_WIDTH.getValue(), GourmetParam.WINDOW_HIGH.getValue()));
		frame.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JLabel label = new JLabel(GourmetMessage.THINK_OF_A_DISH_YOU_LIKE.getValue());
		JButton button = new JButton(GourmetMessage.BUTTON_OK.getValue());
		button.addActionListener(event -> controller.actionButton());

		EmptyBorder emptyBorder = new EmptyBorder(GourmetParam.BORDER_MARGIN_TOP.getValue(), //
				GourmetParam.BORDER_MARGIN_LEFT.getValue(), //
				GourmetParam.BORDER_MARGIN_BOTTOM.getValue(), //
				GourmetParam.BORDER_MARGIN_RIGHT.getValue());

		label.setBorder(new CompoundBorder(label.getBorder(), emptyBorder));

		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.setAlignmentX(Component.CENTER_ALIGNMENT);

		panel.add(label);
		panel.add(button);
		frame.getContentPane().add(panel);
		frame.setVisible(true);
	}
}
