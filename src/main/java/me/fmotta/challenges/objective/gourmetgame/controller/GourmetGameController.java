package me.fmotta.challenges.objective.gourmetgame.controller;

import java.util.LinkedList;
import java.util.Queue;

import me.fmotta.challenges.objective.gourmetgame.constants.GourmetMessage;
import me.fmotta.challenges.objective.gourmetgame.model.BinaryTree;
import me.fmotta.challenges.objective.gourmetgame.model.TreeNode;
import me.fmotta.challenges.objective.gourmetgame.view.ViewMessage;

/**
 * Controlador do jogo gourmet.
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */

public class GourmetGameController {

	/**
	 * raiz da arvore binária.
	 */
	private BinaryTree root;

	/**
	 * Construtor com inicialização dos dados iniciais.
	 */
	public GourmetGameController() {
		this.root = new BinaryTree();
		this.root.add(GourmetMessage.DISH_TYPE_INIT.getValue());
		this.root.addNode(this.root.getRoot(), GourmetMessage.DISH_LASAGNA.getValue(), true);
		this.root.addNode(this.root.getRoot(), GourmetMessage.DISH_CHOCOLATE_CAKE.getValue(), false);
	}

	/**
	 * Ação do botão ok da janela princial.
	 * 
	 * Realiza o processamento e tomada de decisão da busca e adição na arvore
	 * binaria.
	 * 
	 * @see queue https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html
	 */
	public void actionButton() {
		Queue<TreeNode> nodes = new LinkedList<>();
		nodes.add(root.getRoot());
		process(nodes);
	}

	/**
	 * Realiza o processamento de busca e adição na arvore binaria.
	 * 
	 * @param nodes coleção nós da arvore binaria.
	 * @see queue https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html
	 */
	private void process(Queue<TreeNode> nodes) {
		while (!nodes.isEmpty()) {
			TreeNode current = nodes.remove();
			boolean result = ViewMessage.questionTheDishYouThinkAboutIs(current.getData());
			if (result) {
				if (current.getLeft() == null) {
					ViewMessage.infoHit();
				} else {
					nodes.add(current.getLeft());
				}
			} else {
				if (current.getRight() == null) {
					String food = ViewMessage.questionwhatDishDidYouThinkOf();
					String attribute = ViewMessage.questionTheDishYouThinkIsBut(food, current.getData());
					String aux = current.getData();
					current.setData(attribute);
					this.root.addNode(current, food, true);
					this.root.addNode(current, aux, false);
				} else {
					nodes.add(current.getRight());
				}
			}
		}
	}
}
