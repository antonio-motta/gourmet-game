package me.fmotta.challenges.objective.gourmetgame.model;

/**
 * Classe modelo de arvore binária.
 *
 * <br/><br/>
 * Copyright © 2020 Antonio Motta
 */
public class BinaryTree {

	/**
	 * Nós da arvore binaria.
	 */
	TreeNode root;

	/**
	 * Adicionando nó principal (inicial) na arvore.
	 * 
	 * @param value
	 */
	public void add(String value) {
		if (this.root == null) {
			this.root = new TreeNode(value);
		}
	}

	/**
	 * Adicionando nó há e adicionando a arvore binaria raiz.
	 * 
	 * @param parentNode nó
	 * @param data       informação
	 * @param option     <b>true</b> para inserir nó a esquerda - <b>false</b> para
	 *                   inserir a direita.
	 */
	public void addNode(TreeNode parentNode, String data, boolean option) {
		root = this.addTreeNode(parentNode, data, option);
	}

	/**
	 * Retorna nó para adicionar a outro nó na arvore binária.
	 * 
	 * @param parentNode nó
	 * @param data       informação
	 * @param option     <b>true</b> para inserir nó a esquerda - <b>false</b> para
	 *                   inserir a direita.
	 * @return {@link TreeNode} nó de arvore binária.
	 */
	private TreeNode addTreeNode(TreeNode parent, String data, boolean option) {
		if (parent == null) {
			parent = new TreeNode(data);
			return parent;
		} else if (option) {
			parent.setLeft(new TreeNode(data));
		} else {
			parent.setRight(new TreeNode(data));
		}
		return parent;
	}

	/**
	 * Verifica se a raiz principal está vazia, sem nó.
	 * 
	 * @return {@link boolean} true quando a raiz esta vazia.
	 */
	public boolean isEmpty() {
		return root == null;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}
}
