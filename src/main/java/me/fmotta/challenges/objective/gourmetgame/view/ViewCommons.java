package me.fmotta.challenges.objective.gourmetgame.view;

import javax.swing.JOptionPane;

/**
 * Classe de metodos comuns da camada de visualização.
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */
public class ViewCommons {

	/**
	 * Construtor padrão.
	 */
	private ViewCommons() {
	}

	/**
	 * Metodo de implementação JOptionPane do tipo questionário com retorno em sim
	 * ou não.
	 * 
	 * @param title    titulo do dialogo.
	 * @param question messagem do dialogo.
	 * @return resposta do dialogo <b>true</b> para sim ou <b>false</b> para não.
	 */
	public static boolean question(String title, String question) {
		return JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, //
				question, title, JOptionPane.YES_NO_OPTION);
	}

	/**
	 * Metodo de implementação JOptionPane do tipo informativo.
	 * 
	 * @param title   titulo do dialogo.
	 * @param message messagem do dialogo.
	 */
	public static void information(String title, String message) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Metodo de implementação JOptionPane do tipo questionário com retorno de
	 * string do usuário.
	 * 
	 * @param title   titulo do dialogo.
	 * @param message messagem do dialogo.
	 * @return {@link String} resposta do usuário.
	 */
	public static String questionInput(String title, String message) {
		return JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
	}
}
