package me.fmotta.challenges.objective.gourmetgame;

import me.fmotta.challenges.objective.gourmetgame.view.ViewMain;

/**
 * Classe principal de inicialização do jogo.
 *
 * <br/><br/>
 * Copyright © 2020 Antonio Motta
 */
public class GourmetGame {

	public static void main(String[] args) {
		new ViewMain().start();
	}

}
