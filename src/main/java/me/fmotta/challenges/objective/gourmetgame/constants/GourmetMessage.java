package me.fmotta.challenges.objective.gourmetgame.constants;

/**
 * Constantes usada no projeto
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */
public enum GourmetMessage {

	/**
	 * Nome do jogo.
	 */
	NAME_GAME("Jogo Gourmet"), //

	/**
	 * Nome do botão da janela principal.
	 */
	BUTTON_OK("OK"), //

	/**
	 * Messagem de desistência.
	 */
	I_GIVE_UP("Desisto"), //

	/**
	 * Messagem para completar a frase.
	 */
	COMPLETE("Complete"), //

	/**
	 * Messagem de confimação.
	 */
	CONFIRM("Confirm"), //

	/**
	 * Tipo de prato inicial.
	 */
	DISH_TYPE_INIT("massa"), //

	/**
	 * Prato inicial do nó esquerda.
	 */
	DISH_LASAGNA("Lasanha"), //

	/**
	 * Prato inicial do nó a direita.
	 */
	DISH_CHOCOLATE_CAKE("Bolo de Chocolate"), //

	/**
	 * Mensagem da janela inicial.
	 */
	THINK_OF_A_DISH_YOU_LIKE("Pense em um prato que gosta!"), //

	/**
	 * Resposta de tentativa de advinhação.
	 */
	THE_DISH_YOU_THINK_ABOUT_IS("O prato que você pensou é %s?"), //

	/**
	 * Pergunta qual o prato você pensou.
	 */
	WHAT_DISH_DID_YOU_THINK_OF("Qual prato você pensou?"), //

	/**
	 * Pergunta para completar para descobrir qual a diferença entre os pratos.
	 */
	IS_BUT("%s é _____________ mas %s não"), //

	/**
	 * Messagem de acerto.
	 */
	HIT_AGAIN("Acertei de novo!");

	/**
	 * Messagem.
	 */
	private String value;

	/**
	 * Construtor com valor da messagem.
	 * 
	 * @param value messagem que sera exibida.
	 */
	private GourmetMessage(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getValue(Object... parans) {
		return String.format(this.value, parans);
	}

}
