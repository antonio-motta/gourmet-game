package me.fmotta.challenges.objective.gourmetgame.constants;

/**
 * Parametros do jogo.
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */
public enum GourmetParam {

	/**
	 * Largura da janela principal.
	 */
	WINDOW_WIDTH(275), //

	/**
	 * Altura da janela principal.
	 */
	WINDOW_HIGH(120), //

	/**
	 * Margem superior da borda.
	 */
	BORDER_MARGIN_TOP(15), //

	/**
	 * Margem esquerda da borda.
	 */
	BORDER_MARGIN_LEFT(30), //

	/**
	 * Margem inferior da borda.
	 */
	BORDER_MARGIN_BOTTOM(5), //

	/**
	 * Margem direita da borda.
	 */
	BORDER_MARGIN_RIGHT(30);

	/**
	 * valor da constante.
	 */
	private Integer value;

	/**
	 * Construtor padrão com valor.
	 * 
	 * @param value valor do parametro.
	 */
	private GourmetParam(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}