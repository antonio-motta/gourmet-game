package me.fmotta.challenges.objective.gourmetgame.view;

import me.fmotta.challenges.objective.gourmetgame.constants.GourmetMessage;

/**
 * Metodo das messagens da camada de visualização.
 *
 * <br/>
 * <br/>
 * Copyright © 2020 Antonio Motta
 */
public class ViewMessage {

	/**
	 * Construtor padrão.
	 */
	private ViewMessage() {
	}

	/**
	 * Metodo de messagem do informe de aceite.
	 */
	public static void infoHit() {
		ViewCommons.information(GourmetMessage.NAME_GAME.getValue(), GourmetMessage.HIT_AGAIN.getValue());
	}

	/**
	 * Metodo de messagem de pergunta para tentativa de advinhação "O prato que
	 * pensou é".
	 * 
	 * @param data dado para o dialogo.
	 * @return resposta do dialogo <b>true</b> para sim ou <b>false</b> para não.
	 */
	public static boolean questionTheDishYouThinkAboutIs(String data) {
		return ViewCommons.question(GourmetMessage.CONFIRM.getValue(),
				GourmetMessage.THE_DISH_YOU_THINK_ABOUT_IS.getValue(data));
	}

	/**
	 * Metodo de messagem de pergunta "Qual prato você pensou?".
	 * 
	 * @return {@link String} resposta do usuário.
	 */
	public static String questionwhatDishDidYouThinkOf() {
		return ViewCommons.questionInput(GourmetMessage.I_GIVE_UP.getValue(),
				GourmetMessage.WHAT_DISH_DID_YOU_THINK_OF.getValue());
	}

	/**
	 * Metodo de messagem de pergunta "Qual a caracteristica do prato que pensou".
	 * 
	 * @param food caracteristica do prato.
	 * @param dish prato.
	 * 
	 * @return {@link String} resposta do usuário.
	 */
	public static String questionTheDishYouThinkIsBut(String food, String dish) {
		return ViewCommons.questionInput(GourmetMessage.COMPLETE.getValue(),
				GourmetMessage.IS_BUT.getValue(food, dish));
	}

}
