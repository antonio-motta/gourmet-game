# Desafio t�cnico - Jogo Gourmet
Projeto desenvolvido como parte - [desafio t�cnico][desafio-pdf] - do processo de sele��o para para Desenvolvedor Java para a [Objective Solutions][objective-solutions] mediada pela [K2 Partnering][K2-Partnering].
<hr>

## Tecnologias usadas durante desenvolvimento:
| Tecnologia | Descri��o do uso | Vers�o usada |
| ------ | ------ | ------ |
| Java Oracle (JRE-JDK) | Linguagem de Programa��o | 1.8.0_221 | 
| Eclipse IDE | IDE de desenvolvimento | 2019-09 R (4.13.0) | 
| Apache Maven| Gerenciador de depend�ncias| 3.6.2 |
| Git| Controle de vers�o de c�digo| 2.23.0.windows.1 |
| Git Flow| Framework Git para gerenciamento de branchs| Faz parte do Git |
| Java Swing | Framework de GUI Java para Desktop | Faz parte da JDK |

## Implementa��es futuras:
- Testes unit�rios.

[K2-Partnering]: <https://k2partnering.com/pt-br/>
[objective-solutions]: <https://www.objective.com.br/>
[desafio-pdf]: <https://github.com/antonio-motta/gourmet-game/blob/release/v1.0.0/src/main/resources/docs/teste-backend.pdf>